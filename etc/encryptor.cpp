#include<iostream>
#include<sstream>
#include<string>
#include<cstdlib>

using namespace std;

#define TABLE_SIZE 256
#define KEY_SIZE 8

typedef unsigned char int8;

struct rnt {
    
    void init(){
        cout<<"rndtable"<<endl;
        for(size_t x = 0; x < TABLE_SIZE; x++){
            table[x] = rand() % 256;
            cout<<table[x]<<' ';
        }
        cout<<endl;
    }
    
    int8 &operator[](int8 index){
        return table[index];
    }
    
    int8 table[TABLE_SIZE];

};

struct encryptor{
    rnt RN;
    int8 key[KEY_SIZE];
    int8 tag, tag_bit, tag_word;

    void init(){
        RN.init();
        tag = tag_bit = tag_word = 0;
    }
    
    void setKey(int8 newKey[KEY_SIZE]){
        for(int x = 0; x < KEY_SIZE;x++)
            key[x] = newKey[x];
    }

    int8 encrypt(int8 unencrypted){
        int8 encrypted = unencrypted;
        for(int8 subKey = 1; subKey < KEY_SIZE; subKey++){
            encrypted = key[subKey]^encrypted;
            encrypted = RN[encrypted];
        }
        updateTag(encrypted);
        return encrypted;
    }

    int8 updateTag(int8 encrypted){
        //NOTE we could use a bit-address instruction to do this instead
        int8 subk = key[tag_word];
        subk = subk >> (7 - tag_bit);
        //adressed bit is now lowest bit of subk
        tag = tag^(encrypted<<(subk%2));
        //update the tag adress
        tag_bit++;
        if(tag_bit > 7){
            tag_bit = 0;
            tag_word++;
            if(tag_word >= KEY_SIZE)
                tag_word = 0;
        }
        return tag;
    }

    int8 getTag(){
        return tag;
    }
    
};

int main(void){
    encryptor mainEncrypt;
    mainEncrypt.init();
	string input;
	cout<<"please enter a string to encrypt"<<endl;
	cin>>input;

	stringstream instream(input);

	int8 key[KEY_SIZE] = {0,0,0,0,0,0,0,0};

	mainEncrypt.setKey(key);

    //assuming that input(unencrypted) text comes from a global stream, in
    while(!instream.eof()){
        //encrypt until the string is empty
		int8 a = 'a';
		a = mainEncrypt.encrypt(instream.get());
		cout<<a;
	}

	return 0;
}
