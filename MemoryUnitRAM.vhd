--------------------------------------------------------------------------------
-- Entity: MemoryUnit
-- Date:2013-03-29  
-- Author: Jase     
--
-- Description ${cursor}
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity MemoryUnitRAM is
	port  (rst            : in std_logic;
        Addr    : in std_logic_vector(7 downto 0);          --address of read/written cell
        input    : in std_logic_vector(7 downto 0);         --value to be written to *Addr
        writeEnable    : in std_logic;                      --write enable bit
        output         : out std_logic_vector(7 downto 0);  --value *Addr
        clk            : in std_logic
	);
end MemoryUnitRAM;


architecture arch of MemoryUnitRAM is

type memBlock is array(255 downto 0) of std_logic_vector(7 downto 0);
signal mem_sig : memBlock;
begin

    process(Addr,input,writeEnable,clk, rst)
        variable mem   : memBlock;
        variable var_addr   : integer;
 
    
    begin
        --generate variables from std_logic_vectors
        var_addr := conv_integer(Addr);
        if(rising_edge(clk)) then
            if (rst = '1') then
    	        --unencrypted text input
                mem(0)    := X"48";
                mem(1)    := X"69";
                mem(2)    := X"73";
                mem(3)    := X"20";
                mem(4)    := X"6C";
                mem(5)    := X"61";
                mem(6)    := X"70";
                mem(7)    := X"74";
                mem(8)    := X"6F";
                mem(9)    := X"70";
                mem(10)   := X"27";
                mem(11)   := X"73";
                mem(12)   := X"20";
                mem(13)   := X"65";
                mem(14)   := X"6E";
                mem(15)   := X"63";
    			mem(16)   := X"72";
                mem(17)   := X"79";
                mem(18)   := X"70";
                mem(19)   := X"74";
                mem(20)   := X"65";
                mem(21)   := X"64";
                mem(22)   := X"2E";
                mem(23)   := X"20";
                mem(24)   := X"44";
                mem(25)   := X"72";
                mem(26)   := X"75";
                mem(27)   := X"67";    
                mem(28)   := X"20";   
                mem(29)   := X"68";   
                mem(30)   := X"69";    
    			mem(31)   := X"6D";    
                mem(32)   := X"20";   
                mem(33)   := X"61";   
                mem(34)   := X"6E";
                mem(35)   := X"64";    
                mem(36)   := X"20";   
                mem(37)   := X"68";    
                mem(38)   := X"69";  
                mem(39)   := X"74";   
                mem(40)   := X"20";   
                mem(41)   := X"68";  
                mem(42)   := X"69";  
                mem(43)   := X"6D";
                mem(44)   := X"20"; 
    			mem(45)   := X"77";
                mem(46)   := X"69";
                mem(47)   := X"74";   
                mem(48)   := X"68";
                mem(49)   := X"20"; 
                mem(50)   := X"74";
                mem(51)   := X"68";
                mem(52)   := X"69"; 
                mem(53)   := X"73"; 
                mem(54)   := X"20";
                mem(55)   := X"24"; 
    			mem(56)   := X"35"; 
                mem(57)   := X"20";
                mem(58)   := X"77";
                mem(59)   := X"72"; 
                mem(60)   := X"65"; 
                mem(61)   := X"6E"; 
                mem(62)   := X"63";   
                mem(63)   := X"68"; 
                mem(64)   := X"20";   
                mem(65)   := X"75";
                mem(66)   := X"6E"; 
                mem(67)   := X"63";   
                mem(68)   := X"68"; 
                mem(69)   := X"20";   
                mem(70)   := X"75";   
    			mem(71)   := X"6E";    
                mem(72)   := X"74";  
                mem(73)   := X"69";  
                mem(74)   := X"6C";  
                mem(75)   := X"20";    
                mem(76)   := X"68";   
                mem(77)   := X"65";   
                mem(78)   := X"20";   
                mem(79)   := X"74";   
                mem(80)   := X"65";  
                mem(81)   := X"6C";  
                mem(82)   := X"6C";   
                mem(83)   := X"73";  
                mem(84)   := X"20"; 
                mem(85)   := X"74";   
                mem(86)   := X"68";  
                mem(87)   := X"65";   
                mem(88)   := X"20";  
                mem(89)   := X"70";  
                mem(90)   := X"61";  
                mem(91)   := X"73";  
                mem(92)   := X"73";   
                mem(93)   := X"77";   
                mem(94)   := X"6F"; 
                mem(95)   := X"72";   
    			mem(96)   := X"64";  
                mem(97)   := X"2E";    
                mem(98)   := X"00";    
                mem(99)   := X"00";  
                --end of unencrypted text
                --encrypted text output
                mem(100)  := X"00";   
                mem(101)  := X"00";   
                mem(102)  := X"00";  
                mem(103)  := X"00";  
                mem(104)  := X"00";   
                mem(105)  := X"00";   
                mem(106)  := X"00"; 
                mem(107)  := X"00";  
                mem(108)  := X"00";
                mem(109)  := X"00";  
                mem(110)  := X"00"; 
    			mem(111)  := X"00"; 
                mem(112)  := X"00"; 
                mem(113)  := X"00";  
                mem(114)  := X"00";  
                mem(115)  := X"00";  
                mem(116)  := X"00";  
                mem(117)  := X"00";  
                mem(118)  := X"00";  
                mem(119)  := X"00";  
                mem(120)  := X"00";  
                mem(121)  := X"00";    
                mem(122)  := X"00";  
                mem(123)  := X"00";   
                mem(124)  := X"00";  
                mem(125)  := X"00";   
                mem(126)  := X"00";   
                mem(127)  := X"00";    
                mem(128)  := X"00";   
                mem(129)  := X"00";    
                mem(130)  := X"00";    
    			mem(131)  := X"00";  
                mem(132)  := X"00";  
                mem(133)  := X"00";  
                mem(134)  := X"00";  
                mem(135)  := X"00";  
                mem(136)  := X"00";   
                mem(137)  := X"00";   
                mem(138)  := X"00";  
                mem(139)  := X"00";   
                mem(140)  := X"00";    
                mem(141)  := X"00";  
                mem(142)  := X"00";   
                mem(143)  := X"00";  
                mem(144)  := X"00";   
    			mem(145)  := X"00";    
                mem(146)  := X"00";  
                mem(147)  := X"00";   
                mem(148)  := X"00";    
                mem(149)  := X"00";    
                mem(150)  := X"00";   
                mem(151)  := X"00";   
                mem(152)  := X"00";  
                mem(153)  := X"00";   
                mem(154)  := X"00";   
                mem(155)  := X"00";  
    			mem(156)  := X"00";   
                mem(157)  := X"00";    
                mem(158)  := X"00";  
                mem(159)  := X"00";    
                mem(160)  := X"00";    
                mem(161)  := X"00";   
                mem(162)  := X"00";  
                mem(163)  := X"00";    
                mem(164)  := X"00"; 
                mem(165)  := X"00";   
                mem(166)  := X"00";  
                mem(167)  := X"00";   
                mem(168)  := X"00";   
                mem(169)  := X"00";   
                mem(170)  := X"00";    
    			mem(171)  := X"00";   
                mem(172)  := X"00"; 
                mem(173)  := X"00";  
                mem(174)  := X"00";    
                mem(175)  := X"00";   
                mem(176)  := X"00";   
                mem(177)  := X"00";  
                mem(178)  := X"00";   
                mem(179)  := X"00";   
                mem(180)  := X"00";  
                mem(181)  := X"00";   
                mem(182)  := X"00";   
                mem(183)  := X"00";  
                mem(184)  := X"00";   
                mem(185)  := X"00";   
                mem(186)  := X"00";  
                mem(187)  := X"00";  
                mem(188)  := X"00";   
                mem(189)  := X"00";   
                mem(190)  := X"00";   
                mem(191)  := X"00";   
                mem(192)  := X"00";  
                mem(193)  := X"00";  
                mem(194)  := X"00";  
                mem(195)  := X"00";   
    			mem(196)  := X"00";   
                mem(197)  := X"00";    
                mem(198)  := X"00";  
                mem(199)  := X"00";  
                --end of unencrypted text output
                --key input       
                mem(200)  := X"12";   
                mem(201)  := X"34"; 
                mem(202)  := X"56";   
                mem(203)  := X"78";   
                mem(204)  := X"9A";   
                mem(205)  := X"BC";  
                mem(206)  := X"DE";   
                mem(207)  := X"F0"; 
                --end of key  
                mem(208)  := X"00";  
                mem(209)  := X"00";  
                mem(210)  := X"00";  
    			mem(211)  := X"00";  
                mem(212)  := X"00";   
                mem(213)  := X"00";   
                mem(214)  := X"00";   
                mem(215)  := X"00";   
                mem(216)  := X"00";   
                mem(217)  := X"00";  
                mem(218)  := X"00"; 
                mem(219)  := X"00";  
                mem(220)  := X"00";  
                mem(221)  := X"00";  
                mem(222)  := X"00";   
                mem(223)  := X"00";    
                mem(224)  := X"00";   
                mem(225)  := X"00"; 
                mem(226)  := X"00";  
                mem(227)  := X"00";    
                mem(228)  := X"00";   
                mem(229)  := X"00";  
                mem(230)  := X"00";  
    			mem(231)  := X"00";    
                mem(232)  := X"00";   
                mem(233)  := X"00";  
                mem(234)  := X"00";   
                mem(235)  := X"00";   
                mem(236)  := X"00";  
                mem(237)  := X"00";  
                mem(238)  := X"00";    
                mem(239)  := X"00";  
                mem(240)  := X"00";  
                mem(241)  := X"00";   
                mem(242)  := X"00";   
                mem(243)  := X"00";  
                mem(244)  := X"00"; 
    			mem(245)  := X"00";    
                mem(246)  := X"00";   
                mem(247)  := X"00";   
                mem(248)  := X"00";  
                mem(249)  := X"00";   
                mem(250)  := X"00";
                mem(251)  := X"00";
                mem(252)  := X"00";
                mem(253)  := X"00";
                mem(254)  := X"00";
                mem(255)  := X"00";     
        
            elsif( writeEnable = '1') then
                --if writable, write input to *Addr
                mem(var_addr) := input;
            end if;
        end if;
        --constant read *Addr
        output <= mem(var_addr);
        mem_sig <= mem;
        
    end process;

end arch;

