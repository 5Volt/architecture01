--------------------------------------------------------------------------------
-- Entity: BitAddress
-- Date:2013-03-30  
-- Author: Jase     
--
-- Description ${cursor}
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity BitAddress is
    port  (
        input   : in std_logic_vector(7 downto 0);
        bitAddr : in std_logic_vector(2 downto 0);
        ouput   : out std_logic
    );
end BitAddress;

architecture arch of BitAddress is

begin

    process(input, bitAddr)
        variable var_addr : integer;
    begin
        --convert addr to int
        var_addr := conv_integer(bitAddr);
        --constant map ouput bit
        ouput <= input(var_addr);
    end process;

end arch;

