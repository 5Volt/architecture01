LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY ControlUnitStage3 IS
	PORT (
        RegW    : OUT STD_LOGIC;
        Waddr   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        Inst    : IN STD_LOGIC_VECTOR(15 DOWNTO 0)
	);
END ControlUnitStage3;
	
ARCHITECTURE Combinational OF ControlUnitStage3 IS
    SIGNAL O0,O1,O2,O3 : STD_LOGIC;
BEGIN    
    -- Splits opcode into bits
    O0 <= Inst(15);
    O1 <= Inst(14);
    O2 <= Inst(13);
    O3 <= Inst(12);
    
    -- Ports operands straight to outputs
    Waddr <= Inst(11 DOWNTO 8);
    
    -- Fancy logic
    RegW <= (NOT(O0 AND O1 AND NOT O3) AND (NOT(O1 AND O2)) AND (NOT(O2 AND O3)));
    
END Combinational;
