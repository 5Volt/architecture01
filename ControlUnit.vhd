LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY ControlUnit IS
	PORT (
	RegLoad : OUT STD_LOGIC;
	ALUSrc  : OUT STD_LOGIC;
	EQStore : OUT STD_LOGIC;
	BITStore: OUT STD_LOGIC;
	RNStore : OUT STD_LOGIC;
	MEMStore: OUT STD_LOGIC;
	nPC     : OUT STD_LOGIC;
	CZ_sel  : OUT STD_LOGIC;
	Rw      : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	RaddrA  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	RaddrB  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	RegSrc  : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	ALUOp   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	Imm     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	Inst : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	C    : IN STD_LOGIC;
	Z    : IN STD_LOGIC
	);
END ControlUnit;
	
ARCHITECTURE Combinational OF ControlUnit IS
    SIGNAL O0,O1,O2,O3 : STD_LOGIC ;
    SIGNAL opcode : STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal cpi : std_logic;
BEGIN
    
    -- Stores opcode
    opcode <= Inst(15 DOWNTO 12);
    
    -- Splits opcode into bits
    O0 <= Inst(15);
    O1 <= Inst(14);
    O2 <= Inst(13);
    O3 <= Inst(12);
    
    -- Ports operands straight to outputs
    Rw <= Inst(11 DOWNTO 8);
    RaddrA <= Inst(11 DOWNTo 8) WHEN opcode = "1101" ELSE --addi
    Inst(11 DOWNTO 8) WHEN opcode = "1100" ELSE -- cpi 
    Inst(7 DOWNTO 4);
    RaddrB <= Inst(3 DOWNTO 0);
    Imm <= Inst(7 DOWNTO 0);
    
    -- BRB And C OR BREQ And Z
    CZ_sel <= (O0 AND NOT O1 AND NOT O2 AND NOT O3 AND C) OR (O0 AND NOT O1 AND NOT O2 AND O3 AND Z) OR (O0 AND NOT O1 AND O2 AND NOT O3);
    
    -- Fancy logic
    RegLoad <= (NOT(O0 AND O1 AND NOT O3) AND (NOT(O1 AND O2)) AND (NOT(O2 AND O3)));
    
    -- CBIT
    BITStore <= NOT O0 AND NOT O1 AND O2 AND O3;
    -- CPI OR ADDI
    ALUSrc <= (O0 AND O1 AND NOT O2 AND NOT O3) OR (O0 AND O1 AND NOT O2 AND O3);
    
    -- CPI
    EQStore <= O0 AND O1 AND NOT O2 AND NOT O3;
    
    -- STR
    RNStore <= NOT O0 AND O1 AND O2 AND O3;
    
    -- STM
    MEMStore <= NOT O0 AND O1 AND O2 AND NOT O3;
    
    -- RET    ...and JMP?
    nPC <= (O0 AND NOT O1 AND O2 AND O3); --OR  (O0 AND NOT O1 AND O2 AND NOT O3);
    
    -- ADD OR CPI OR ADDI
    ALUOp(1) <= (NOT O0 AND NOT O1 AND NOT O2 AND O3) OR (O0 AND O1 AND (NOT O2) AND (NOT O3)) OR (O0 AND O1 AND NOT O2 AND O3);
    
    -- LSL OR CPI
    ALUOp(0) <= (NOT O0 AND NOT O1 AND O2 AND NOT O3) OR (O0 AND O1 AND (NOT O2) AND (NOT O3));
    
    -- BRB OR BREQ OR JMP OR LDM
    RegSrc(1) <= (O0 AND NOT O1 AND NOT O2 AND NOT O3) OR (O0 AND NOT O1 AND NOT O2 AND O3) OR (O0 AND NOT O1 AND O2 AND NOT O3) OR (NOT O0 AND O1 AND NOT O2 AND NOT O3);
    
    -- BRB OR BREQ OR JMP OR LDR
    RegSrc(0) <= (O0 AND NOT O1 AND NOT O2 AND NOT O3) OR (O0 AND NOT O1 AND NOT O2 AND O3) OR (O0 AND NOT O1 AND O2 AND NOT O3) OR (NOT O0 AND O1 AND NOT O2 AND O3);
    
    cpi <= (O0 AND NOT O1 AND O2 AND NOT O3);
    
END Combinational;
