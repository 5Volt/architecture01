--------------------------------------------------------------------------------
-- Entity: ALU
-- Date:2013-03-30  
-- Author: Jase     
--
-- Description ${cursor}
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity ALU is
    port  (
        func    : in std_logic_vector(1 downto 0);
        Ra, Rb  : in std_logic_vector(7 downto 0);
        output  : out std_logic_vector(7 downto 0);
        zero    : out std_logic
    );
end ALU;



architecture arch of ALU is
    signal preout : std_logic_vector(7 downto 0);
begin

    process(func, Ra, Rb, preout)
    begin
        if(func = "00") then
            --func 0 = xor
            preout <= Ra XOR Rb;
        elsif(func = "01") then
            --func 1 = LSL
            preout <= Ra(6 downto 0)&'0';
        elsif(func = "10") then
            --func 2 = Add
            preout <= (Ra) + ( Rb);
        else
            --func 3 = and
            preout <= Ra - Rb;
        end if;
        
        --calculate zero flag (saving to status register implemented seperately)
	   if(preout = "00000000") then
	      zero <= '1';
	   else
	      zero <= '0';
       end if;

--        zero <= '1' when (preout = "00000000") else '0';
        
    end process;
	 
	output <= preout;
	
end arch;

