--------------------------------------------------------------------------------
-- Entity: MemoryUnit
-- Date:2013-03-29  
-- Author: Jase     
--
-- Description ${cursor}
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity MemoryUnit is
	port  ( rst            : in std_logic;
        Addr    : in std_logic_vector(7 downto 0);          --address of read/written cell
        input    : in std_logic_vector(7 downto 0);         --value to be written to *Addr
        writeEnable    : in std_logic;                      --write enable bit
        output         : out std_logic_vector(7 downto 0);  --value *Addr
        clk            : in std_logic
	    
	);
end MemoryUnit;


architecture arch of MemoryUnit is

type memBlock is array(255 downto 0) of std_logic_vector(7 downto 0);
begin

    process(Addr,input,writeEnable,clk, rst)
    variable mem   : memBlock;
    variable var_addr   : integer;
    
    begin
        --generate variables from std_logic_vectors
        var_addr := conv_integer(Addr);
		--reset to RN table
        if (rising_edge(clk)) then
            if(rst = '1') then
                mem(0)    := X"34";
                mem(1)    := X"5E";
                mem(2)    := X"6A";
                mem(3)    := X"8D";
                mem(4)    := X"73";
                mem(5)    := X"42";
                mem(6)    := X"24";
                mem(7)    := X"81";
                mem(8)    := X"A0";
                mem(9)    := X"55";
                mem(10)   := X"28";
                mem(11)   := X"4A";
                mem(12)   := X"3F";
                mem(13)   := X"AE";
                mem(14)   := X"A1";
                mem(15)   := X"95";
    			mem(16)   := X"33";
                mem(17)   := X"C4";
                mem(18)   := X"E0";
                mem(19)   := X"7F";
                mem(20)   := X"D3";
                mem(21)   := X"9F";
                mem(22)   := X"DF";
                mem(23)   := X"E7";
                mem(24)   := X"31";
                mem(25)   := X"F0";
                mem(26)   := X"B1";
                mem(27)   := X"D4";    
                mem(28)   := X"72";   
                mem(29)   := X"FB";   
                mem(30)   := X"80";    
    			mem(31)   := X"FE";    
                mem(32)   := X"69";   
                mem(33)   := X"8C";   
                mem(34)   := X"FD";
                mem(35)   := X"6B";    
                mem(36)   := X"AD";   
                mem(37)   := X"00";    
                mem(38)   := X"E1";  
                mem(39)   := X"94";   
                mem(40)   := X"B2";   
                mem(41)   := X"FF";  
                mem(42)   := X"23";  
                mem(43)   := X"3E";
                mem(44)   := X"FC"; 
    			mem(45)   := X"D2";
                mem(46)   := X"EF";
                mem(47)   := X"54";   
                mem(48)   := X"49";
                mem(49)   := X"5D"; 
                mem(50)   := X"F1";
                mem(51)   := X"60";
                mem(52)   := X"C3"; 
                mem(53)   := X"9E"; 
                mem(54)   := X"D5";
                mem(55)   := X"74"; 
    			mem(56)   := X"25"; 
                mem(57)   := X"E6";
                mem(58)   := X"AA";
                mem(59)   := X"30"; 
                mem(60)   := X"D6"; 
                mem(61)   := X"01"; 
                mem(62)   := X"A2";   
                mem(63)   := X"4B"; 
                mem(64)   := X"71";   
                mem(65)   := X"22";
                mem(66)   := X"C5"; 
                mem(67)   := X"AB";   
                mem(68)   := X"BB"; 
                mem(69)   := X"7E";   
                mem(70)   := X"B3";   
    			mem(71)   := X"B0";    
                mem(72)   := X"D1";  
                mem(73)   := X"6C";  
                mem(74)   := X"AF";  
                mem(75)   := X"40";    
                mem(76)   := X"29";   
                mem(77)   := X"82";   
                mem(78)   := X"68";   
                mem(79)   := X"AC";   
                mem(80)   := X"0C";  
                mem(81)   := X"93";  
                mem(82)   := X"5F";   
                mem(83)   := X"48";  
                mem(84)   := X"8B"; 
                mem(85)   := X"21";   
                mem(86)   := X"02";  
                mem(87)   := X"CF";   
                mem(88)   := X"3D";  
                mem(89)   := X"75";  
                mem(90)   := X"DE";  
                mem(91)   := X"35";  
                mem(92)   := X"D0";   
                mem(93)   := X"2F";   
                mem(94)   := X"43"; 
                mem(95)   := X"96";   
    			mem(96)   := X"70";  
                mem(97)   := X"A9";    
                mem(98)   := X"53";    
                mem(99)   := X"8E";  
                mem(100)  := X"4C";   
                mem(101)  := X"20";   
                mem(102)  := X"61";  
                mem(103)  := X"06";  
                mem(104)  := X"B4";   
                mem(105)  := X"92";   
                mem(106)  := X"CE"; 
                mem(107)  := X"D7";  
                mem(108)  := X"2A";
                mem(109)  := X"32";  
                mem(110)  := X"8A"; 
    			mem(111)  := X"BC"; 
                mem(112)  := X"0B"; 
                mem(113)  := X"67";  
                mem(114)  := X"BA";  
                mem(115)  := X"A4";  
                mem(116)  := X"6D";  
                mem(117)  := X"3A";  
                mem(118)  := X"47";  
                mem(119)  := X"18";  
                mem(120)  := X"1F";  
                mem(121)  := X"AB";    
                mem(122)  := X"26";  
                mem(123)  := X"83";   
                mem(124)  := X"41";  
                mem(125)  := X"C6";   
                mem(126)  := X"5C";   
                mem(127)  := X"DD";    
                mem(128)  := X"03";   
                mem(129)  := X"0D";    
                mem(130)  := X"76";    
    			mem(131)  := X"2E";  
                mem(132)  := X"A8";  
                mem(133)  := X"EE";  
                mem(134)  := X"E2";  
                mem(135)  := X"3C";  
                mem(136)  := X"E8";   
                mem(137)  := X"89";   
                mem(138)  := X"1E";  
                mem(139)  := X"4D";   
                mem(140)  := X"0F";    
                mem(141)  := X"91";  
                mem(142)  := X"05";   
                mem(143)  := X"62";  
                mem(144)  := X"36";   
    			mem(145)  := X"17";    
                mem(146)  := X"8F";  
                mem(147)  := X"2B";   
                mem(148)  := X"6E";    
                mem(149)  := X"9C";    
                mem(150)  := X"97";   
                mem(151)  := X"52";   
                mem(152)  := X"12";  
                mem(153)  := X"B5";   
                mem(154)  := X"66";   
                mem(155)  := X"CD";  
    			mem(156)  := X"56";   
                mem(157)  := X"D8";    
                mem(158)  := X"C7";  
                mem(159)  := X"7D";    
                mem(160)  := X"A7";    
                mem(161)  := X"04";   
                mem(162)  := X"16";  
                mem(163)  := X"ED";    
                mem(164)  := X"1D"; 
                mem(165)  := X"77";   
                mem(166)  := X"A3";  
                mem(167)  := X"CB";   
                mem(168)  := X"BD";   
                mem(169)  := X"DC";   
                mem(170)  := X"E3";    
    			mem(171)  := X"88";   
                mem(172)  := X"E9"; 
                mem(173)  := X"90";  
                mem(174)  := X"B9";    
                mem(175)  := X"B6";   
                mem(176)  := X"11";   
                mem(177)  := X"9D";  
                mem(178)  := X"07";   
                mem(179)  := X"3B";   
                mem(180)  := X"84";  
                mem(181)  := X"51";   
                mem(182)  := X"5A";   
                mem(183)  := X"4E";  
                mem(184)  := X"2C";   
                mem(185)  := X"1C";   
                mem(186)  := X"58";  
                mem(187)  := X"39";  
                mem(188)  := X"9B";   
                mem(189)  := X"7C";   
                mem(190)  := X"0E";   
                mem(191)  := X"65";   
                mem(192)  := X"78";  
                mem(193)  := X"44";  
                mem(194)  := X"19";  
                mem(195)  := X"27";   
    			mem(196)  := X"63";   
                mem(197)  := X"C1";    
                mem(198)  := X"13";  
                mem(199)  := X"37";         
                mem(200)  := X"A6";   
                mem(201)  := X"C8"; 
                mem(202)  := X"98";   
                mem(203)  := X"6F";   
                mem(204)  := X"46";   
                mem(205)  := X"15";  
                mem(206)  := X"10";   
                mem(207)  := X"D9";   
                mem(208)  := X"EA";  
                mem(209)  := X"B7";  
                mem(210)  := X"EC";  
    			mem(211)  := X"87";  
                mem(212)  := X"BE";   
                mem(213)  := X"1B";   
                mem(214)  := X"DA";   
                mem(215)  := X"C2";   
                mem(216)  := X"F2";   
                mem(217)  := X"FA";  
                mem(218)  := X"79"; 
                mem(219)  := X"F9";  
                mem(220)  := X"50";  
                mem(221)  := X"0A";  
                mem(222)  := X"F4";   
                mem(223)  := X"E4";    
                mem(224)  := X"F6";   
                mem(225)  := X"7B"; 
                mem(226)  := X"CA";  
                mem(227)  := X"F7";    
                mem(228)  := X"F3";   
                mem(229)  := X"2D";  
                mem(230)  := X"BF";  
    			mem(231)  := X"F8";    
                mem(232)  := X"99";   
                mem(233)  := X"F5";  
                mem(234)  := X"B8";   
                mem(235)  := X"38";   
                mem(236)  := X"85";  
                mem(237)  := X"1A";  
                mem(238)  := X"EB";    
                mem(239)  := X"5B";  
                mem(240)  := X"E5";  
                mem(241)  := X"C9";   
                mem(242)  := X"DB";   
                mem(243)  := X"C0";  
                mem(244)  := X"09"; 
    			mem(245)  := X"14";    
                mem(246)  := X"59";   
                mem(247)  := X"57";   
                mem(248)  := X"45";  
                mem(249)  := X"64";   
                mem(250)  := X"9A";
                mem(251)  := X"A5";
                mem(252)  := X"08";
                mem(253)  := X"7A";
                mem(254)  := X"86";
                mem(255)  := X"4F";
        
            elsif(writeEnable = '1') then
                --if writable, write input to *Addr
                mem(var_addr) := input;
            end if;
        end if;
        --constant read *Addr
        -- NOTE apparently this helps with not jumping randomly, can replace with clocked output if necessary
        output <= mem(var_addr);
		  
	    -- the following are probe signals (for simulation purpose)  -- Added this in
        --sig_rn_mem <= mem;
        
    end process;

end arch;

