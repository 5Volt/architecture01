--------------------------------------------------------------------------------
-- Entity: DataPath
-- Date:2013-03-30  
-- Author: Jase     
--
-- Description ${cursor}
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity DataPath is 
    port (  rst          : in std_logic;      
            regLoad      : in std_logic;      --input signals
            ALUSrc       : in std_logic;
            EQStore      : in std_logic;
            BITStore     : in std_logic;
            RNStore      : in std_logic;
            MEMStore     : in std_logic;
            pc_src       : in std_logic;
            cz_sel       : in std_logic;
            Waddr        : in std_logic_vector(3 downto 0);
            RaddrA       : in std_logic_vector(3 downto 0);
            RaddrB       : in std_logic_vector(3 downto 0);
            RegSrc       : in std_logic_vector(1 downto 0);
            ALUOp        : in std_logic_vector(1 downto 0);
            Immediate    : in std_logic_vector(7 downto 0);
            Instruction  : out std_logic_vector(15 downto 0);
            C            : out std_logic;
            Z            : out std_logic;
            clk          : in std_logic
        );
end DataPath;

architecture arch of DataPath is
    
    component ALU is 
        port (  func    : in std_logic_vector(1 downto 0);
                Ra, Rb  : in std_logic_vector(7 downto 0);
                output  : out std_logic_vector(7 downto 0);
                zero    : out std_logic );
    end component;
    
    component MemoryUnit is 
        port  ( rst          : in std_logic;
                Addr         : in std_logic_vector(7 downto 0);   --address of read/written cell
                input        : in std_logic_vector(7 downto 0);   --value to be written to *Addr
                writeEnable  : in std_logic;                      --write enable bit
                output       : out std_logic_vector(7 downto 0);  --value *Addr
                clk          : in std_logic
                );
    end component;  
	 
    component MemoryUnitRAM is 
        port  ( rst          : in std_logic;
                Addr         : in std_logic_vector(7 downto 0);   --address of read/written cell
                input        : in std_logic_vector(7 downto 0);   --value to be written to *Addr
                writeEnable  : in std_logic;                      --write enable bit
                output       : out std_logic_vector(7 downto 0);  --value *Addr
                clk          : in std_logic
                );  
    end component;
    
    component RegisterFile is 
        port  ( rst            : in std_logic;
                RaddrA, RaddrB : in std_logic_vector(3 downto 0);     --address of registers to read
                Ra, Rb         : out std_logic_vector(7 downto 0);    --value of registers read
                input          : in std_logic_vector(7 downto 0);     --input value for load register
                WaddrA         : in std_logic_vector(3 downto 0);     --adress of register to write to
                WriteEnabled   : in std_logic;                        --allows input to be written to *Waddr
                clk            : in std_logic                         --raising writes input to *Waddr
   );
    end component;
    
    component BitAddress is
    port  ( input   : in std_logic_vector(7 downto 0);
            bitAddr : in std_logic_vector(2 downto 0);
            ouput   : out std_logic
    );
    end component;
    
    component PCUnit is
    port (  rst         :   in std_logic;
            pc_src      :   in std_logic;    --selects which input goes to PC
            cz_sel      :   in std_logic;
            Immediate   :   in std_logic_vector(7 downto 0);    --immediate offset value for jump
            regIn       :   in std_logic_vector(7 downto 0);    --input from register file can be restored to PC
            regOut      :   out std_logic_vector(7 downto 0);   --ouput back to register(for jmpl)
           -- Inst        :   out std_logic_vector(15 downto 0);   --output from instruction memory
            clk         :   in std_logic;
            PC_Val      :   out std_logic_vector (7 downto 0);
            addr_out  : in  std_logic_vector(7 downto 0)
    );
    end component;
    
    component InstructionMemory is port ( 
                        rst         : in std_logic;
                        address     : in std_logic_vector(7 downto 0);
                        instruction : out std_logic_vector(15 downto 0);
                        clk         : in std_logic       -- input clock, xx MHz.
                    );
    end component;
    
    component program_counter is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(7 downto 0);
           addr_out : out std_logic_vector(7 downto 0) );
    end component;
    
    signal Ra, Rb, ALUInB, PCInB, ALUOut, PCOut, RNOut, MEMOut, RegIn, PC_Val, addr_out : std_logic_vector(7 downto 0);
    signal ALUZ, CBIT : std_logic;
    
begin
    
    Registers : RegisterFile port map (rst, RaddrA, RaddrB, Ra, Rb, RegIn, Waddr, RegLoad, clk);
    BitCheck : BitAddress port map(Ra, Rb(2 downto 0), CBIT );
    RN : MemoryUnit port map(rst, Ra, Rb, RNStore, RNOut, clk); 
    RAM : MemoryUnitRAM port map(rst, Ra, Rb, MEMStore, MEMOut, clk);
    myALU : ALU port map(ALUOp, Ra, ALUInB, ALUOut, ALUZ);
    PC : PCUnit port map( rst, pc_src, cz_sel, Immediate , Rb, PCOut, clk, PC_Val, addr_out);
    mem: InstructionMemory port map(rst, addr_out, Instruction, clk);
    pcCount : program_counter port map ( reset => rst, clk => clk, addr_in  => PC_Val, addr_out => addr_out ); 

    
    -- register load source, ALUInB, 
    RegIn <= ALUOut when (RegSrc = "00") else
            RNOut when (RegSrc = "01") else
            MEMOut when (RegSrc = "10") else
            PCOut;
    --ALU second input
    ALUInB <= Immediate when (ALUSrc = '1') else
            Rb;
            
    --PCInB <=  Immediate when (pc_src = '1') else
     --         Immediate when (cz_sel = '1') else
     --       Rb;     
            
    --save status bits on clock edge
    process(clk, EQStore, BITStore) is
    begin
        if(rising_edge(clk)) then
            if(BITStore = '1') then
                C <= CBIT;
            else
            --    C <= '0';
            end if;
            
            if(EQStore = '1') then
                Z <= ALUZ;
            --else
            --    Z <= '0';
            end if;
        end if;
    end process;

end arch;
