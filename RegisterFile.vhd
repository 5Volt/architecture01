--------------------------------------------------------------------------------
-- Entity: RegisterFile
-- Date:2013-03-29  
-- Author: Jrab911     
--
-- Description: Procedural "Register File" component for single sycle encryption processor
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity RegisterFile is
	port  (
        rst            : in std_logic;
		RaddrA, RaddrB : in std_logic_vector(3 downto 0);     --address of registers to read
		Ra, Rb         : out std_logic_vector(7 downto 0);    --value of registers read
		input          : in std_logic_vector(7 downto 0);     --input value for load register
		WaddrA         : in std_logic_vector(3 downto 0);     --adress of register to write to
		WriteEnabled   : in std_logic;                        --allows input to be written to *Waddr
		clk            : in std_logic                         --raising writes input to *Waddr
	);
end RegisterFile;

architecture arch of RegisterFile is
       type regSet is array(0 to 15) of std_logic_vector(7 downto 0); 
       signal sig_regfile : regSet;
begin

    process(RaddrA, RaddrB,input,WaddrA, clk, WriteEnabled) 
        variable registers      : regSet;
        variable var_writeAddr, 
        var_addrA, var_addrB    : integer;
    begin
        --integer variables for array dereferencing
        var_writeAddr := conv_integer(WaddrA);
        var_addrA := conv_integer(RaddrA);
        var_addrB := conv_integer(RaddrB);
        --writes can only occur on rising edge
        if(falling_edge(clk)) then
            if (rst = '1') then
                registers := (others => X"00");
        
            elsif(WriteEnabled = '1') then
                registers(var_writeAddr) := input;
            end if;
        end if;
        
        --keep dedicated value registers
        registers(0) := X"00";  
        registers(1) := X"01";
        
        --read output registers
        Ra <= registers(var_addrA);
        Rb <= registers(var_addrB);
        
        sig_regfile <= registers;
    end process;
  

end arch;

