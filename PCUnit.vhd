--------------------------------------------------------------------------------
-- Entity: PCUnit
-- Date:2013-04-04  
-- Author: jrab911     
--
-- Description ${cursor}
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity PCUnit is
	port  (
	    rst         :   in std_logic;
        pc_src      :   in std_logic;    --selects which input goes to PC
        cz_sel      :   in std_logic;
        Immediate   :   in std_logic_vector(7 downto 0);    --immediate offset value for jump
        regIn       :   in std_logic_vector(7 downto 0);    --input from register file can be restored to PC
        regOut      :   out std_logic_vector(7 downto 0);   --ouput back to register(for jmpl)
        --Inst        :   out std_logic_vector(15 downto 0);   --output from instruction memory
        clk         :   in std_logic;
        PC_Val      :   out std_logic_vector(7 downto 0);
        addr_out    : in std_logic_vector(7 downto 0)
	);
end PCUnit;

architecture arch of PCUnit is

signal PCImmediate, PCInc, cz_out,PC_Val_sig   :   std_logic_vector(7 downto 0);

begin
    PCInc <= addr_out + 1;
    PCImmediate <= addr_out + Immediate;

    process(Immediate, PCInc, PCImmediate, cz_sel, pc_src, regIn, clk) is
    
    begin
    

    --crossbar switch to decide next PC source
    if(cz_sel = '0') then
        cz_out <= PCInc;
        regOut <= PCImmediate;
    else
        cz_out <= PCImmediate;
        regOut <= PCInc;
    end if;
    
    --assign new PC value on rising clock edges
    --if(rising_edge(clk)) then
        --choose source for new PC value
        if(pc_src = '0') then
            PC_Val <= cz_out;
        else
            PC_Val <= regIn;
        end if;
    --end if;
    
    end process;
    
    
end arch;

