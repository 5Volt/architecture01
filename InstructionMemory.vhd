--------------------------------------------------------------------------------
-- Entity: InstructionMemory
-- Date:2013-04-04  
-- Author: jrab911     
--
-- Description ${cursor}
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
 
entity InstructionMemory is
    port (  rst         : in std_logic;
            address     : in std_logic_vector(7 downto 0);
            instruction : out std_logic_vector(15 downto 0);
            clk         : in std_logic       -- input clock, xx MHz.
    );
end InstructionMemory;

architecture arch of InstructionMemory is
type ImemBlock is array(0 to 255) of std_logic_vector(15 downto 0);
signal inst_sig : ImemBlock;

begin

    process(address, clk, rst) is
    variable mem : ImemBlock;
    variable var_addr   : integer;

    begin
        --generate integer from address
        var_addr := conv_integer(address);
        if(rising_edge(clk)) then
            if (rst = '1') then
                -- main:
                mem(0)   := X"1A00";    --- add  $cn, $r0, $r0
                mem(1)   := X"D300";    --- addi $ri, 0x00
                mem(2)   := X"D464";    --- addi $rc, 0x64
                mem(3)   := X"D5C8";    --- addi $kp, 0xC8
                mem(4)   := X"1700";    --- add  $tgb, $r0, $r0
                
                -- main loop:  
                mem(5)   := X"4930";    --- ldm  $in, $ri, $r0
                mem(6)   := X"C900";    --- cpi  $in, 0x00
                mem(7)   := X"920C";    --- breq $rl, exit
                mem(8)   := X"A20C";    --- jmp  $rl, encrypt
                mem(9)   := X"A228";    --- jmp  $rl, mask
                mem(10)  := X"6049";    --- stm  $r0, $rc, $in
                mem(11)  := X"4930";    --- ldm  $in, $ri, $r0
                mem(12)  := X"A214";    --- jmp  $rl, tag
                mem(13)  := X"1331";    --- add  $ri, $ri, $r1
                mem(14)  := X"1441";    --- add  $rc, $rc, $r1
                mem(15)  := X"1AA1";    --- add  $cn, $cn, $r1
                mem(16)  := X"CA64";    --- cpi  $cn, 0x64
                mem(17)  := X"9E02";    --- breq $t3, exit
                mem(18)  := X"AEF3";    --- jmp  $t3, main_loop
                
                -- exit:
                mem(19)  := X"A200";    --- jmp  $rl, exit
                
                -- encrypt: 
                mem(20)  := X"1B50";    --- add  $t0, $kp, $ $r0
                mem(21)  := X"1C00";    --- add  $t1, $r0, $r0
                mem(22)  := X"1850";    --- add  $tgw, $kp, $r0
                
                -- encrypt_loop:
                mem(23)  := X"4DB0";    --- ldm  $t2, $t0, $r0
                mem(24)  := X"099D";    --- xor  $in, $in, $t2
                mem(25)  := X"5990";    --- ldr  $in, $in, $r0
                mem(26)  := X"1BB1";    --- add  $t0, $t0, $r0
                mem(27)  := X"1CC1";    --- add  $t1, $t1, $r1
                mem(28)  := X"CC08";    --- cpi  $t1, 0x08
                mem(29)  := X"9E02";    --- breq $t3, return
                mem(30)  := X"AEF9";    --- jmp  $t3, encrypt_loop
                
                -- return: 
                mem(31)  := X"B002";    --- ret $rl
                
                -- tag: 
                mem(32)  := X"4B80";    -- ldm  $t0, $tgw, $r0
                mem(33)  := X"30B7";    -- cbit $r0, $t0, $tgb
                mem(34)  := X"8E06";    -- brb  $t3, shift
                mem(35)  := X"0669";    -- xor  $tg, $tg, $in
                mem(36)  := X"1771";    -- add  $tgb, $tgb, $r1
                mem(37)  := X"C708";    -- cpi  $tgb, 0x08
                mem(38)  := X"9E04";    -- breq $t3, inc_word
                mem(39)  := X"B002";    -- ret  $rl
                
                -- shift: 
                mem(40)  := X"2991";    -- lsl  $in, $in, $r1
                mem(41)  := X"B00E";    -- ret  $t3
                
                -- inc_word:
                mem(42)  := X"1700";    -- add  $tgb, $r0, $r0
                mem(43)  := X"1881";    -- add  $tgw, $tgw, $r1
                mem(44)  := X"C808";    -- cpi  $tgw, 0x08 
                mem(45)  := X"9F02";    -- breq $t4, rst_word
                mem(46)  := X"B00E";    -- ret $t3
                
                -- rst_word:
                mem(47)  := X"1800";    -- add $tgw, $r0, $r0
                mem(48)  := X"B00F";    -- ret $t4
                
                -- mask:
                mem(49)  := X"1B00";    --- add $t0, $r0, $r0
                mem(50)  := X"DB07";    -- addi $t0, 0x07 
                mem(51)  := X"309B";    -- cbit $r0, $in, $t1
                mem(52)  := X"8E02";    -- brb $t3, mask_xor
                mem(53)  := X"B002";    -- ret, $rl
                
                -- mask_xor
                mem(54)  := X"1C00";    -- add $t1, $r0, $r0 
                mem(55)  := X"DC80";    -- addi $t1, 0x80
                mem(56)  := X"099C";    -- xor $in, $in, $t1
                mem(57)  := X"B00E";    -- ret $t3
                
            end if;
         end if;
            
        instruction <= mem(var_addr);
        inst_sig <= mem;
            

    end process;

end arch;

