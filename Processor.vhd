LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY Processor IS
PORT (
	rst : IN STD_LOGIC;
	clk : IN STD_LOGIC);
END Processor;

ARCHITECTURE Structural OF Processor IS

COMPONENT DataPath is 
      port( rst          : in std_logic;      
            regLoad      : in std_logic;      --input signals
            ALUSrc       : in std_logic;
            EQStore      : in std_logic;
            BITStore     : in std_logic;
            RNStore      : in std_logic;
            MEMStore     : in std_logic;
            pc_src       : in std_logic;
            cz_sel       : in std_logic;
            Waddr        : in std_logic_vector(3 downto 0);
            RaddrA       : in std_logic_vector(3 downto 0);
            RaddrB       : in std_logic_vector(3 downto 0);
            RegSrc       : in std_logic_vector(1 downto 0);
            ALUOp        : in std_logic_vector(1 downto 0);
            Immediate    : in std_logic_vector(7 downto 0);
            Instruction  : out std_logic_vector(15 downto 0);
            C            : out std_logic;
            Z            : out std_logic;
            clk          : in std_logic
    );
END COMPONENT;

COMPONENT ControlUnit IS
    PORT (  RegLoad : OUT STD_LOGIC;
            ALUSrc  : OUT STD_LOGIC;
            EQStore : OUT STD_LOGIC;
            BITStore: OUT STD_LOGIC;
            RNStore : OUT STD_LOGIC;
            MEMStore: OUT STD_LOGIC;
            nPC     : OUT STD_LOGIC;
            CZ_sel  : OUT STD_LOGIC;
            Rw      : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            RaddrA  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            RaddrB  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            RegSrc  : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
            ALUOp   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
            Imm     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            Inst : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            C    : IN STD_LOGIC;
            Z    : IN STD_LOGIC
    );
END COMPONENT;

SIGNAL RegL, ALUS, EQSTR, CSTR, RnStore, MemStore, NPC, CZ: STD_LOGIC;
SIGNAL RaddrW, RaAD, RbAD : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL REGSrc, AluOP : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL Immed : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL Instruct : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL CB, ZB   : STD_LOGIC;

BEGIN

data_path : DataPath PORT MAP(
            rst => rst,      
            regLoad => regL,
            ALUSrc => ALUS,
            EQStore => EQSTR,
            BITStore => CSTR,
            RnStore => RNStore,
            MemStore=> MEMStore,
            pc_src => NPC,
            cz_sel => CZ,
            Waddr => RaddrW,
            RaddrA => RaAD,
            RaddrB => RbAD,
            RegSrc => REGSrc,
            ALUOP => AluOP,
            Immediate => Immed,
            Instruction => Instruct,
            C => CB,
            Z => ZB,
            clk => clk
			);

control_unit: ControlUnit PORT MAP(
            RegLoad => regL,
            ALUSrc => ALUS,
            EQStore => EQSTR,
            BITStore => CSTR,
            RnStore => RNStore,
            MemStore=> MEMStore,
            NPC => nPC,
            CZ_sel => CZ,
            Rw => RaddrW,
            RaddrA => RaAD,
            RaddrB => RbAD,
            REGSrc => RegSrc,
            ALUOp => AluOP,
            Imm => Immed,
            Inst => Instruct,
            C => CB,
            Z => ZB
			);

END Structural;


